## WEB APPLICATION

---

**Run application**

1. Go to the directory of project.

2. Enter in console command **npm install.**

3. Enter in console command **npm start.**

4. Go to URL **[localhost:3000](http://127.0.0.1:3000/).**

---
