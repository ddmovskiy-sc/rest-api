'use strict';

const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const pathImg = path.resolve(__dirname, 'src/resources/img');

const PATHS = {
  app: path.join(__dirname, './src/client'),
  build: path.join(__dirname, './dist'),
};

module.exports = {
 
  entry: PATHS.app,
  output: {
    path: PATHS.build,
    filename: 'bundle.js', 
    library: 'renderApplication',
    libraryTarget: 'umd'
    
  },
   module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        include: pathImg,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'])
  ]
};