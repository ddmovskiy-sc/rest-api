'use strict';

import _ from 'lodash';
import XLSX from 'xlsx';

export const parseXLSX = dataFromExcel => {
  const data = [];
  const results = _.groupBy(dataFromExcel, author => {
    return author.firstName+"-"+author.lastName;
  });
  for (let key in results) {
    const object = {};
    const authorBooks = [];
    results[key].forEach(author => {
      object.first_name = author.firstName;
      object.last_name = author.lastName;
    const obj = {};
    author.books.split(',').forEach(book => {
    const item = book.split(":");
    if (item[0].trim() === 'title'){
      obj[item[0].trim()] = item[1].trim();
    } else {
      obj[item[0].trim()] = parseInt(item[1].trim(), 10);
    }
    });
    authorBooks.push(obj);
    });
     object.books = authorBooks;
     data.push(object);
    }
  return data;
};

export const exportToXLSX = authors => {
  const exportData = [['firstName', 'lastName', 'books']];   
  authors.forEach(author => {
    const books = author.books;
    books.forEach(book => {
      exportData.push([author.first_name, author.last_name, `title: ${book.title}, isbn: ${book.isbn}`]);
    });
  });
  const ws = XLSX.utils.aoa_to_sheet(exportData);
  const wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws);
  XLSX.writeFile(wb, "export.xlsx");
}

export const readXLSX = file => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = e => {
      const workbook = XLSX.read(e.target.result, { type: 'binary' });
      workbook.SheetNames.forEach(sheetName => {
      resolve(XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]));
      });
    };
    reader.onerror = reject;
  });
}

export const compareBy = condition => {
  return (author1, author2) => {
    const result = author1[condition.key] < author2[condition.key] ?
     1 : (author1[condition.key] > author2[condition.key] ? -1 : 0);
    return condition.isAscSortDirection ? result : - result;
  };
}
