'use strict';

import { LOAD_ALL_AUTHORS,
         IMPORT_AUTHORS,
         DELETE_AUTHOR_BY_ID,
         FILTER_AUTHORS,
         SORT_AUTHORS } from '../constants/actionsTypes';
import { INITIAL_STATE_AUTHORS_REDUCER } from '../constants/initialStateReducers';


export const authorsReducer = (state = INITIAL_STATE_AUTHORS_REDUCER, action) => {
  
  switch(action.type) {
    case LOAD_ALL_AUTHORS:
      return [...action.payload]
    case IMPORT_AUTHORS:
      return [...action.payload]
    case DELETE_AUTHOR_BY_ID:
      return [...action.payload]
    case FILTER_AUTHORS:
      return [...action.payload]
    case SORT_AUTHORS:
      return [...action.payload]
    default:
      return state
  }
}