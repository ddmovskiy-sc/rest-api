'use strict';

import { combineReducers } from "redux";
import { authorsReducer } from './authorsReducer';
import { booksReducer } from './booksReducer';
import { authorUpdateReducer } from './authorUpdateReducer';


export const rootReducer = combineReducers({
  authors: authorsReducer,
  books: booksReducer,
  authorUpdate: authorUpdateReducer
});



