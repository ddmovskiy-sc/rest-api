'use strict';

import { LOAD_AUTHOR_BY_ID, UPDATE_AUTHOR } from '../constants/actionsTypes';
import { INITIAL_STATE_AUTHOR_UPDATE_REDUCER } from '../constants/initialStateReducers';


export const authorUpdateReducer = (state = INITIAL_STATE_AUTHOR_UPDATE_REDUCER, action) => {
  
  switch(action.type) {
    case LOAD_AUTHOR_BY_ID:
      return { ...action.payload }
    case UPDATE_AUTHOR:
      return { ...action.payload }
    default:
      return state
  }
}
