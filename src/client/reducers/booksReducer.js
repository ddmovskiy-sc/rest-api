'use strict';

import { LOAD_BOOKS_BY_AUTHOR_ID } from '../constants/actionsTypes';
import { INITIAL_STATE_BOOKS_REDUCER } from '../constants/initialStateReducers';

 
export const booksReducer = (state = INITIAL_STATE_BOOKS_REDUCER, action) => {
  
  switch(action.type) {
    case LOAD_BOOKS_BY_AUTHOR_ID:
      return [ ...action.payload ]
    default:
      return state
  }
}
