'use strict';

const React = require("react");
const { render } =  require('react-dom');
const { Home, CreateAuthor, UpdateAuthor, AuthorBooks } = require('./components/pages');
const { BrowserRouter, Route } = require("react-router-dom");
const { Provider } = require("react-redux");
const { store } = require('./configureStore');


module.exports = (node) => {
  const App = () => (
    <BrowserRouter>
      <div>
        <Route path="/books/:authorId" component={AuthorBooks} />
        <Route exact path="/" component={Home} />
        <Route exact path="/add" component={CreateAuthor} />
        <Route path="/edit/:authorId/" component={UpdateAuthor} />
      </div>
    </BrowserRouter>
  );
  render(
    <Provider store={store}>
      <App />
    </Provider>,
    node
  );  
}

