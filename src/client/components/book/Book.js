"use strict";

import React, { Component } from "react";
import PropTypes from "prop-types";

export default class BookForm extends Component {

  render() {
    
    return (
      <div>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            className="form-control"
            name="title"
            placeholder="Enter title"
            value={this.props.operation === "update" ?
              this.props.authorBook.title : 
              this.props.appendBooks[this.props.index].title
            }
            onChange={event => this.props.operation === "update" ?
              this.props.onUpdateBookChange({ ...this.props.book, title: event.target.value, index: this.props.index }) :
              this.props.onCreateBookChange({ ...this.props.book, title: event.target.value })} />
        </div>
        <div className="form-group">
          <label htmlFor="isbn">Isbn</label>
          <input
            type="text"
            className="form-control"
            name="isbn"
            placeholder="Enter isbn"
            value={this.props.operation === "update" ?
              this.props.authorBook.isbn : 
              this.props.appendBooks[this.props.index].isbn
            }
            onChange={event => this.props.operation === "update" ?
              this.props.onUpdateBookChange({ ...this.props.book, isbn: event.target.value, index: this.props.index }) :
              this.props.onCreateBookChange({ ...this.props.book, isbn: event.target.value })} />
        </div>
      </div>
    );
  }
}

BookForm.propTypes = {
  onUpdateBookChange: PropTypes.func,
  onCreateBookChange: PropTypes.func,
  authorBooks: PropTypes.array,
  appendBooks: PropTypes.array,
  operation: PropTypes.string,
  index: PropTypes.number
};