'use strict';

import React, { Component } from "react";
import PropTypes from "prop-types";

export default class BooksList extends Component {

  constructor() {
    super();
    this.state = {
      isOpen: false
    };
  }

  handleInfoBook() {
    this.setState({
      isOpen: this.state.isOpen ? false : true
    });
  }

  render() {
    return (
      <div>
        <div className="card">
          <div className="card-header">
            <h5 className="mb-0">
              <button
                className="btn btn-link"
                type="button"
                data-toggle="collapse"
                aria-expanded={this.state.isOpen}
                onClick={this.handleInfoBook.bind(this)}
              >
                 {this.props.book.title}
              </button>
            </h5>
          </div>
          <div className={`collapse ${this.state.isOpen ? "show" : "hide"}`}>
            <div className="card-body">
              <div>
                <b>Title: </b>
                {this.props.book.title}
              </div>
              <div>
                <b>Isbn: </b>
                {this.props.book.isbn}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BooksList.propTypes = {
  book: PropTypes.shape({
    title: PropTypes.string,
    // isbn: PropTypes.number
  })
};