"use strict";

import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import "./../../styles/common.css";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import  {deleteAuthorById} from '../../actions';

class AuthorsList extends Component {

  render() {

    return (
      <div>
        <table className="table table-striped table-hover table-bordered" id='table-to-xls'>
          <thead>
            <tr>
              <th>#</th>
              <th onClick={() => this.props.onSortBy("first_name")}>
                First name<div className={`fa fa-sort-${this.props.arrowDirection.fieldName} arrow-position`} />
              </th>
              <th onClick={() => this.props.onSortBy("last_name")}>
                Last name<div className={`fa fa-sort-${this.props.arrowDirection.fieldLastName} arrow-position`} />
              </th>
              <th>Books</th>
              <th colSpan="2">Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.props.authors.map((author, index) => (
             <tr key={index} className='item'>
                <td>{index + 1}</td>
                <td>{author.first_name}</td>
                <td>{author.last_name}</td>
                <td>
                  <Link to={{
                    pathname: `/books/${author.id}`,
                    state: {
                      author: `${author.first_name.charAt(0)}. ${author.last_name}'s`
                    }
                  }}>
                    Show books
                  </Link>
                </td>
                <td>
                  <div className="btn-group">
                    <Link to={{ pathname: `/edit/${author.id}` }}>
                      <button type="button" className="btn btn-info btn-size">
                        Edit
                      </button>
                    </Link>
                    <button
                      type="button"
                      className="btn btn-dark btn-size"
                      value={author.id}
                      onClick={() => this.props.deleteAuthorById(author.id, this.props.authors )}>
                      Delete
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authors: state.authors
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ deleteAuthorById: deleteAuthorById }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(AuthorsList);


AuthorsList.propTypes = {
  deleteAuthorById: PropTypes.func.isRequired,
  authors: PropTypes.array.isRequired
};