"use strict";

import React, { Component } from "react";
import PropTypes from "prop-types";

export default class AuthorForm extends Component {
  render() {
    return (
      <div>
        <div className="form-group">
          <label htmlFor="firstname">First name</label>
          <input
            type="text"
            className="form-control"
            name="name"
            placeholder="Enter name"
            value={
              this.props.operation === "update" ? 
              this.props.activeAuthor.first_name :
              this.props.currentAuthor.name
            }
            onChange={event => this.props.operation === "update" ?
              this.props.onUpdateAuthorChange({ ...this.props.author, first_name: event.target.value }) :
              this.props.onCreateAuthorChange({ ...this.props.author, name: event.target.value })
            }
          />
        </div>

        <div className="form-group">
          <label htmlFor="lastname">Last name</label>
          <input
            type="text"
            className="form-control"
            name="lastname"
            placeholder="Enter lastname"
            value={this.props.operation === "update" ?
              this.props.activeAuthor.last_name :
              this.props.currentAuthor.lastname
            }
            onChange={event => this.props.operation === "update" ?
              this.props.onUpdateAuthorChange({ ...this.props.author, last_name: event.target.value }) :
              this.props.onCreateAuthorChange({ ...this.props.author, lastname: event.target.value })
            }
          />
        </div>
      </div>
    );
  }
}

AuthorForm.propTypes = {
  onUpdateAuthorChange: PropTypes.func,
  onCreateAuthorChange: PropTypes.func,
  author: PropTypes.shape({
    name: PropTypes.string,
    lastname: PropTypes.string
  }),
  currentAuthor: PropTypes.shape({
    name: PropTypes.string,
    lastname: PropTypes.string
  })
};