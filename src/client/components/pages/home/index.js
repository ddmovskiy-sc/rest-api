import React, { Component } from 'react';
import '../../../styles/common.css';
import logo from '../../../../resources/img/logo.png';
import { Link } from "react-router-dom";
import { exportToXLSX } from '../../../util';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import AuthorsList from '../../author/AuthorsList';
import { loadAllAuthors, importAuthors, filterAuthors, sortAuthors } from "../../../actions";

class Home extends Component {

constructor(props) {
  super(props);
  this.state = {
    choiceFile: {},
    sortedField: null,
    isAscSortDirection: false,
    sortDirection: {
      fieldName: 'down',
      fieldLastName: 'down'
    }
  }
}

handleSortBy(key) {
  if (this.state.sortedField !== key) {
    this.setState({
      sortedField: key,
      isAscSortDirection: false
    })
    this.props.sortAuthors(this.props.authors, { key: key, isAscSortDirection: this.state.isAscSortDirection });
  } else {
      this.setState({
        sortedField: key,
        isAscSortDirection: this.state.isAscSortDirection === true ? false : true,
        sortDirection: {
          fieldName: (key === 'first_name') ? ((this.state.sortDirection.fieldName === 'up') ? 'down' : 'up') :
          this.state.sortDirection.fieldName,
          fieldLastName: (key === 'last_name') ? ((this.state.sortDirection.fieldLastName) === 'up' ? 'down' : 'up') :
          this.state.sortDirection.fieldLastName
        }
      })
      this.props.sortAuthors(this.props.authors, { key: key, isAscSortDirection: this.state.isAscSortDirection });
    }
}

handleChooseFile(event) {
  this.setState({ choiceFile: event.target.files[0] });
}

componentDidMount() {
  this.props.loadAllAuthors();
}

render() {
  return (
    <div>
      <nav className="navbar navbar-light bg-light justify-content-between">
        <a href="http://127.0.0.1:3000" className="navbar-brand"><img className='logo' src={logo} alt="Logo"/></a>
        <div className="form-inline">
          <input onKeyUp={(event) => this.props.filterAuthors(event, this.props.authors)} className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
          <Link to='/add'><button className="btn btn-info my-2 my-sm-0" type="submit">Add author</button></Link> 
        </div> 
      </nav>
      <AuthorsList
         onSortBy={this.handleSortBy.bind(this)}
         arrowDirection = {this.state.sortDirection} />
      <div className="input-group">
        <div className="custom-file">
          <input type="file" onChange={this.handleChooseFile.bind(this)} className="custom-file-input" id="inputGroup"/>
          <label className="custom-file-label" htmlFor="inputGroup">Choose file</label>
        </div>
        <div className="input-group-append">
          <button onClick={() => this.props.handleImportData(this.state.choiceFile)} className="btn btn-outline-secondary">Import</button>
          <button onClick={event => exportToXLSX(this.props.authors)} className="btn btn-outline-secondary">Export</button>
        </div>
      </div>   
    </div>
    )
  }
}

const mapStateToProps = state => {
  return { authors: state.authors }
}

const matchDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      loadAllAuthors: loadAllAuthors,
      importAuthors: importAuthors,
      filterAuthors: filterAuthors,
      sortAuthors: sortAuthors
    },
    dispatch
  )
}

export default connect(mapStateToProps, matchDispatchToProps)(Home);

