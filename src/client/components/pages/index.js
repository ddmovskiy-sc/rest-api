'use strict';

export { default as CreateAuthor } from './createAuthor';
export { default as Home } from './home';
export { default as UpdateAuthor } from './updateAuthor';
export { default as AuthorBooks } from './authorBooks'; 