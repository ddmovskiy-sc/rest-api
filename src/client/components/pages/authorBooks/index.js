'use strict';

import React, { Component } from 'react';
import '../../../styles/common.css';
import { withRouter } from 'react-router';
import { Link } from "react-router-dom";
import AuthorBooksList from '../../book/BooksList';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loadBooksByAuthorId } from '../../../actions';  

class BooksList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      books: []
    }; 
  };

  componentDidMount() {
    this.props.loadBooksByAuthorId(this.props.match.params.authorId);
  }

  render() {
       
    return (
     
      <div>
        <div className='link-style'><Link to={`/`}>Home</Link></div>
        <div className='text-center h3'>{this.props.location.state.author} books</div>
        <div className="accordion component-size">
          {this.props.books.map((book, index) =>
            <AuthorBooksList key={index} book={book} />
          )}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    books: state.books
  }
}

const matchDispatchToProps = (dispatch) => {
  return bindActionCreators({ loadBooksByAuthorId: loadBooksByAuthorId }, dispatch);
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(BooksList)); 






