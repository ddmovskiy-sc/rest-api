"use strict";

import React, { Component } from "react";
import "./css/style.css";
import "../../../styles/common.css";
import BookForm from "../../book/Book";
import AuthorForm from "../../author/Author";
import { Error, StatusSave } from "./messages";
import { Link } from "react-router-dom";
import  { saveAuthors } from '../../../actions/commonActions';

export default class CreateAuthor extends Component {

  constructor() {
    super();
    this.state = {
      author: { name: "", lastname: "" },
      appendBooks: [{ title: "", isbn: "" }],
      messages: { error: false, statusSave: false }
    };
  }

  handleAuthorChange(authorInputField) {
    const property = Object.keys(authorInputField)[0];
    const currentAuthor = this.state.author;
    currentAuthor[property] = authorInputField[property];
    this.setState({
      author: currentAuthor,
      messages: { error: false, statusSave: false }
    });
  }

  handleBookChange(bookInputFiled) {
    const property = Object.keys(bookInputFiled)[0];
    let currentBooks = this.state.appendBooks;
    let currentBook = currentBooks[currentBooks.length - 1];
    currentBook[property] = bookInputFiled[property];
    this.setState({
      appendBooks: currentBooks,
      messages: { 
        error: false,
        statusSave: false
      }
    });
  }

  handleAddFormBook() {
    const currentBooks = this.state.appendBooks;
    currentBooks.push({ title: "", isbn: "" });
    this.setState({
      error: false,
      appendBooks: currentBooks
    });
  }

  handleSave(event) {
    event.preventDefault();
    let author = {};
    author.first_name = this.state.author.name;
    author.last_name = this.state.author.lastname;
    const appendBooks = this.state.appendBooks;
    if (appendBooks[appendBooks.length - 1].title === "") {
      this.setState({ appendBooks: this.state.appendBooks.splice(-1, 1) });
    }
    author.books = this.state.appendBooks;
    saveAuthors([ author ]);
    this.setState({messages: {error: false, statusSave: true }});
  }

  render() {
    return (
      <div className="link-style">
        <Link to={`/`}>Home</Link>
        <div className="component-size">
          <div className="text-center h3">Create new author</div>
          <AuthorForm
            onCreateAuthorChange={this.handleAuthorChange.bind(this)}
            currentAuthor={this.state.author} operation="create" />
          {this.state.appendBooks.map((item, index) => (
            <BookForm
              key={index}
              operation="create"
              index={index}
              appendBooks={this.state.appendBooks}
              onCreateBookChange={this.handleBookChange.bind(this)} />
          ))}
          {this.state.messages.error && <Error />}
          {this.state.messages.statusSave && <StatusSave />}
          <div className="block-inline">
            <button
              type="button"
              className="btn btn-info btn-sm btn-size btn-add"
              onClick={this.handleAddFormBook.bind(this)}>
              Add book
            </button>
            <button
              type="button"
              className="btn btn-info btn-sm btn-size"
              onClick={this.handleSave.bind(this)}>
              Save
            </button>
          </div>
        </div>
      </div>
    );
  }
}

 