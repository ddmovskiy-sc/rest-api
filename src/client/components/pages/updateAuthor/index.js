"use strict";

import React, { Component } from "react";
import { withRouter } from "react-router";
import AuthorForm from "../../author/Author";
import BookForm from "../../book/Book";
import { Link } from "react-router-dom";
import "./css/style.css";
import "../../../styles/common.css";
import ResultEditMessage from "./messages/ResultEditMessage";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { loadAuthorById, updateAuthor } from "../../../actions";


class UpdateAuthor extends Component {
  
  constructor() {
    super();
    this.state = {
      bookAfterUpdate: {
        title: "",
        isbn: ""
      },
      messages: { statusEdit: false }
    };
  }

  componentDidMount() {
    this.props.loadAuthorById(this.props.match.params.authorId);
  }

  handleAuthorChange(changedField) {
    const propertyName = Object.keys(changedField)[0];
    const currentAuthor = this.props.author;
    currentAuthor[propertyName] = changedField[propertyName];
    this.props.updateAuthor(currentAuthor);
  }

  handleAuthorSaveChange() {
    fetch(`http://localhost:3000/api/authors/${this.props.match.params.authorId}`,
      {
        method: "PUT",
        body: JSON.stringify({
          first_name: this.props.author.first_name,
          last_name: this.props.author.last_name
        }),
        headers: { "Content-Type": "application/json" }
      }
    )
      .then(results => {
        this.setState({ messages: { statusEdit: true } });
        return results.json();
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleBookChange(changedField) {
    const property = Object.keys(changedField)[0];
    const currentBook = this.state.bookAfterUpdate;
     if (property === 'title') {
       currentBook[property] = changedField[property];
       this.setState({ bookAfterUpdate: currentBook});
     } else {
         currentBook[property] = changedField[property];
         this.setState({ bookAfterUpdate: currentBook});
     }
    const currentAuthor = this.props.author;
    currentAuthor.books[changedField.index][property] = changedField[property];
    this.props.updateAuthor(currentAuthor);
  }

  handleBookSaveChange(event) {
    fetch(`http://localhost:3000/api/books/${event.target.value}`, {
      method: "PUT",
      body: JSON.stringify({
        title: this.state.bookAfterUpdate.title,
        isbn: this.state.bookAfterUpdate.isbn
      }),
      headers: { "Content-Type": "application/json" }
    })
      .then(results => {
         return results.json();
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {

    return (
      <div className="link-style">
        <Link to={`/`}>Home</Link>
        <div className="component-size">
          <div className="text-center h2">
            Edit {this.props.author.first_name.charAt(0)}. {this.props.author.last_name}
          </div>
          <div className="shadow-lg p-3 mb-5 bg-white rounded">
            <AuthorForm
              activeAuthor={this.props.author}
              operation="update"
              onUpdateAuthorChange={this.handleAuthorChange.bind(this)} />
            <button
              type="button"
              className="btn btn-info btn-sm btn-align"
              onClick={this.handleAuthorSaveChange.bind(this)}>
              Save changes
            </button>
            <div className="msg-margin-top">
              {this.state.messages.statusEdit && <ResultEditMessage />}
            </div>
          </div>
          <div className="text-center h2">books</div>
          {this.props.author.books.map((book, index) => (
            <div key={index} className="shadow-lg p-3 mb-5 bg-white rounded">
              <BookForm
                index={index}
                authorBook={book}
                operation="update"
                onUpdateBookChange={this.handleBookChange.bind(this)} />
              <button
                type="button btn-link"
                className="btn btn-info btn-sm btn-align"
                value={book.id}
                onClick={this.handleBookSaveChange.bind(this)}>
                Save changes
              </button>
            </div>
          ))}
       </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    author: state.authorUpdate
  }
}

const matchDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      loadAuthorById: loadAuthorById,
      updateAuthor: updateAuthor
    },
    dispatch);
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(UpdateAuthor));
