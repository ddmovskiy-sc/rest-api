'use strict';

export const INITIAL_STATE_BOOKS_REDUCER = [{
  title: '',
  isbn: ''
}];

export const INITIAL_STATE_AUTHOR_UPDATE_REDUCER = {
	first_name: '',
  last_name: '',
  books: [{
    title: '',
    isbn: ''
  }]
};
 
export const INITIAL_STATE_AUTHORS_REDUCER = [{
	first_name: '',
  last_name: '',
  books: [{
    title: '',
    isbn: ''
  }]
}];

