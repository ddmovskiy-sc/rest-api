'use strict';

import { LOAD_ALL_AUTHORS,
         LOAD_BOOKS_BY_AUTHOR_ID,
         FILTER_AUTHORS,
         SORT_AUTHORS,
         LOAD_AUTHOR_BY_ID,
         UPDATE_AUTHOR,
         DELETE_AUTHOR_BY_ID } from '../constants/actionsTypes';
import { compareBy } from '../util';


export const loadAllAuthors = () => {
  return dispatch => {
    fetch('http://localhost:3000/api/authors')
      .then(response => response.json())
      .then(authors => {
        dispatch({
        type: LOAD_ALL_AUTHORS,
        payload: authors
        })
      })
      .catch(error => { throw(error) });
  }
}

export const loadBooksByAuthorId = authorId => {
  return dispatch => {
    fetch(`http://localhost:3000/api/authors/${authorId}/books`)
      .then(books => books.json())
      .then(books => {
         dispatch({
           type: LOAD_BOOKS_BY_AUTHOR_ID,
           payload: books
         })
      })
      .catch(error => { throw(error) });
  }
}

let allAuthors = [];
export const filterAuthors = (event, authors) => {
  return dispatch => {
    if (allAuthors.length === 0) {
      allAuthors = authors;
    }
    if (authors.length < allAuthors.length) {
      authors = allAuthors;
    }
    const searchTerm = event.target.value.toLowerCase();
    const authorsAfterFilter = authors.filter(authors =>
      authors.first_name.toLowerCase().indexOf(searchTerm) !== -1 ||
      authors.last_name.toLowerCase().indexOf(searchTerm) !== -1);
    dispatch({
      type: FILTER_AUTHORS,
      payload: authorsAfterFilter
    })
    if (event.target.value.length === 0) {
      dispatch({
        type: FILTER_AUTHORS,
        payload: allAuthors
      })
    }
  }
}

export const sortAuthors = (currentAuthors, conditionCompare) => {
  console.log(conditionCompare);  
  return dispatch => {
    const authorsCopy = [ ...currentAuthors ];
    authorsCopy.sort(compareBy(conditionCompare));  
    dispatch({
      type: SORT_AUTHORS,
      payload: authorsCopy
    })
  }
}

export const loadAuthorById = (authorId) => {
  return dispatch => {
    fetch(`http://localhost:3000/api/authors/${authorId}`)
      .then(results => {
        return results.json();
      })
      .then(authorWithBooks => {
         dispatch({
           type: LOAD_AUTHOR_BY_ID,
           payload: authorWithBooks
         })
      })
      .catch(error => { throw(error) });
  }
}

export const updateAuthor = author => {
  return dispatch => {
    dispatch({
      type: UPDATE_AUTHOR,
      payload: author
    })
  }
}

export const deleteAuthorById = (authorId, state) => {
  return dispatch => {
    fetch(`http://localhost:3000/api/authors/${authorId}`, { method: 'DELETE' })
      .then(() => {
        return state.filter(author => {
          return author.id !== parseInt(authorId, 10);
        });
      })
      .then(authorsAfterRemove => {
        dispatch({
          type: DELETE_AUTHOR_BY_ID,
          payload: authorsAfterRemove
        });
      })
      .catch(error => { throw(error) });   
  }
}

export const saveAuthors = authors => {
  authors.forEach(author => {
    fetch("http://localhost:3000/api/authors", {
      method: "POST",
      body: JSON.stringify(author),
      headers: { "Content-Type": "application/json" }
    })
      .then(result => {
         return result;
      })
      .catch(error => { throw(error) });
  });
}


