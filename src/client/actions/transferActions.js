'use strict';

import { parseXLSX, readXLSX } from '../util';
import { saveAuthors } from './commonActions';
import { IMPORT_AUTHORS } from '../constants/actionsTypes';
  
export const importAuthors = fileXLSX => {
  return dispatch => {
    readXLSX(fileXLSX)
      .then(data => {
        return parseXLSX(data);
      })
      .then(authors => {
        saveAuthors(authors);
        return authors;
      })
      .then(authors => {
        dispatch({
          type: IMPORT_AUTHORS,
          payload: authors
        }) 
      }).
      catch(error => { throw(error) });
  }
}
