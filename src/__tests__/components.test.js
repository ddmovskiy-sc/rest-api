'use strict';

import React from 'react';
import Enzyme, { configure, shallow, mount } from 'enzyme';
import sinon from 'sinon';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import  AuthorsList  from '../client/components/author/AuthorsList';
import  { CreateAuthor, Home, AuthorBooks }  from '../client/components/pages';



describe('Tests of components', () => {

  let items = '';
 
  before(() => {
    items = [
      { first_name: 'name1', last_name: 'lastname1' },
      { first_name: 'name2', last_name: 'lastname2' },
      { first_name: 'name3', last_name: 'lastname3' }
    ];
  });

  after(() => {
    items = null;
  });

  it('renders three list-authors in AuthorsList', () => {
    const wrapper = shallow(<AuthorsList authors={items} onSort = {()=>{}} onRemove={()=>{}} arrowDirection={{fieldName: 'down', fieldLastName: 'down'}} />);
    expect(wrapper.find('.item')).to.have.lengthOf(3);
  });

  it('renders btn `Add author` in Home', () => {
    const wrapper = shallow(<AuthorsList authors={items} onSort = {()=>{}} onRemove={()=>{}} arrowDirection={{fieldName: 'down', fieldLastName: 'down'}} />);
    expect(wrapper.contains(<button className="btn btn-info my-2 my-sm-0" type="submit">Add author</button>)).to.equal(false);
  });

  it('renders an `logo` in Home', () => {
    const wrapper = shallow(<Home />);
    expect(wrapper.find('.logo')).to.have.lengthOf(1);
  });

  it('redders one form for book in CreateAuthor', () => {
    const wrapper = shallow(<CreateAuthor />);
    expect(wrapper.state('appendBooks').length).to.equal(1);
  });

  it('redders two buttons for form in CreateAuthor', () => {
    const wrapper = shallow(<CreateAuthor />);
    const countButtons = wrapper.findWhere(n => n.type() === 'button').length;
    expect(countButtons).to.equal(2);
  });

  it('increment state appendBooks after click by one', () => {
    const onButtonClick = sinon.spy();
    const wrapper = shallow(<CreateAuthor onButtonClick={onButtonClick} />);
    wrapper.find('.btn-add').simulate('click');
    expect(wrapper.state('appendBooks').length).to.equal(2);
  });
  
  

});