'use strict';

import { assert } from 'chai';
import { parseXLSX, exportToXLSX, readXLSX } from '../util';
import fs from 'fs';

describe("Application tests", () => {

    describe('Tests of util methods', () => {

      let inputData = undefined;
      let expected1 = undefined;
      let expected2 = undefined;

      before(() => {
        inputData = [
          { firstName: 'name1', lastName: 'sirname1', books: 'title: testBook1, isbn: 12345' },
          { firstName: 'name2', lastName: 'sirname2', books: 'title: testBook2, isbn: 678910' }
        ];
        expected1 = [
          { first_name: 'name1', last_name: 'sirname1', books: [{ title: 'testBook1', isbn: 12345 }] },
          { first_name: 'name2', last_name: 'sirname2', books: [{ title: 'testBook2', isbn: 678910 }] }
        ];
        expected2 = [
          { first_name: 'name1', last_name: 'sirname1', books: [{ title: 'testBook1', isbn: 12345 }] },
          { first_name: 'name2', last_name: 'sirname2', books: [ 'title: testBook2, isbn: 678910' ] }
        ]
      });

      after(() => {
        inputData = null;
        expected1 = null;
        expected2 = null;
        fs.unlink(process.cwd()+'/export.xlsx', (err) => {
          return err; 
        });
      });
      
      it('parseXLSX. Actual and expected values should be equal', () => {
        const actual = parseXLSX(inputData);      
        assert.deepEqual(actual, expected1);
        assert.notDeepEqual(actual, expected2);
      });

      it('exportToXLSX. Actual and expected values should be equal', () => {
        exportToXLSX(expected1);
        assert.equal(true, fs.existsSync(process.cwd() + '/export.xlsx'));
      });

      it('readXLSX. Actual and expected values should be equal', () => {
        if (fs.existsSync(process.cwd() + '/export.xlsx')) {
          readXLSX(process.cwd() + '/export.xlsx').
            then((result) => assert.equal(inputData, result)).
            catch(error => { return error });
        } else {
          exportToXLSX(expected1);
          readXLSX(process.cwd() + '/export.xlsx').
           then((result) => assert.equal(inputData, result)).
           catch(error => { return error });
        }
      });
    });

  });