'use strict';
module.exports = (sequelize, DataTypes) => {
  const Author = sequelize.define('Author', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING
  }, {});
  Author.associate = function(models) {
    Author.hasMany(models.Book, { as: 'books', onDelete: 'cascade', hooks: true });
  };
  return Author;
};