'use strict';

const getBookDao = require('../dao/books');

module.exports = async (app) => {
  const bookDao = await getBookDao();

  app.route('/api/books/:bookId?').
    get((req, res) => {
      if (req.params.bookId) {
        bookDao.findById(req.params.bookId).
          then(books => {
            (books === null) ? res.status(404).json("No content") : res.json(books);
          }).
          catch(error => res.status(500).json(error));
      } else {
        bookDao.findAll().
          then(books => {
            (books === null) ? res.status(404).json("No content") : res.json(books);
          }).
          catch(error => res.status(500).json(error));
      }
    }).
    put((req, res) => {
      bookDao.updateById(req.params.bookId, req.body).
        then(updatedBook => {
          (updatedBook === null) ? res.status(404).json("No content") : res.json(updatedBook);
        }).
        catch(error => res.status(500).json(error));
    }).
    delete((req, res) => {
      bookDao.deleteById(req.params.bookId).
        then(statusDelete => {
          (statusDelete === 1) ? res.json("Deleted successfully") : res.status(404).json("No content");
        }).
        catch(error => res.status(500).json(error));
    });

  app.route('/api/authors/:authorId/books/:bookId?').
    get((req, res) => {
      bookDao.findAllByAuthor(req.params.authorId).
        then(books => {
          (books.length === 0) ? res.status(404).json("No content") : res.json(books);
        }).
        catch(error => res.status(500).json(error));
    }).
    post((req, res) => {
      bookDao.addBook(req.params.authorId, req.body).
        then(newBook => {
          res.status(201).json(newBook);
        }).
        catch(error => res.status(500).json(error));
    });
};
