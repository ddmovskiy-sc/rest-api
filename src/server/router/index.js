'use strict';

const routes = [
  require('./author'),
  require('./book'),
  require('./static')
];

module.exports = (app) => {
  return routes.forEach((route) => {
    route(app);
  });
};
