'use strict';

const getAuthorDao = require('../dao/author');

module.exports = async (app) => {
  const authorDao = await getAuthorDao();

  app.route('/api/authors/:id?').
    get((req, res) => {
      if (req.params.id) {
         authorDao.findById(req.params.id, req.query.include).
         then(author => {
            (author === null) ? res.status(404).json("No content") : res.json(author);
         }).
          catch(error => res.status(500).json(error));
      } 
      if (Object.keys(req.query).length !== 0) {
        authorDao.findByField(req.query).
        then(author => (author === null) ? res.status(404).json("No content") : res.json(author)).
        catch(error => res.status(500).json(error));
      } else {
        authorDao.findAll().
        then(author => (author === null) ? res.status(404).json("No content") : res.json(author)).
        catch(error => res.status(500).json(error));
      }      
    }).
    post((req, res) => {
      authorDao.addAuthor(req.body).
        then(newAuthor => {
          res.status(201).json(newAuthor);
        }).
        catch(error => res.status(500).json(error));
    }).
    put((req, res) => {
      if (req.params.id) {
        authorDao.updateById(req.params.id, req.body).
          then(changedAuthor => {
            (changedAuthor === null) ? res.status(404).json("No content") : res.json(changedAuthor)
          }).
          catch(error => res.status(500).json(error));
      }
    }).
    delete((req, res) => {
      if (req.params.id) {
        authorDao.deleteById(req.params.id).
          then((statusDelete) => {
            (statusDelete === 1) ? res.json("Deleted successfully") : res.status(404).json("No content");
          }).
          catch(error => res.status(500).json(error));
      }
    });
};