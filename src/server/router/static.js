'use strict';

const express = require('express');
const path = require('path');

module.exports = async (app) => {

    app.use(express.static('dist'));

    app.get( ['/', '/add', '/edit/:id', '/books/:id'], (req, res) => {
         res.sendFile(path.normalize(path.join(__dirname, '..','..', '..', 'public', 'index.html'))); 
        });
}

