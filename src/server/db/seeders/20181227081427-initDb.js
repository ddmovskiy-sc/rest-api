'use strict';

const authors = require('../../../resources/authors');

module.exports = {
    up: (queryInterface, Sequelize) => {

      return Promise.all(authors.map(author => {
        return queryInterface.bulkInsert('Authors', [
          {
            first_name: author.first_name,
            last_name: author.last_name,
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ]).
          then(id => {
            let booksList = [];
              author.books.forEach(book => {
                booksList.push(
                  {
                    title: book.title,
                    isbn: book.isbn,
                    authorId: id,
                    createdAt: new Date(),
                    updatedAt: new Date()
                  }
                );
            })
            return queryInterface.bulkInsert('Books', booksList);   
          }).
          catch(error => console.error(error));
      }))
    },

    down: (queryInterface, Sequelize) => {
    
      return Promise.all(authors.map(author => {
        return queryInterface.bulkDelete('Authors', 
          [ 
          {first_name: author.first_name},
          {last_name: author.last_name}
          ]     
        ).then((result) => {
           return Promise.all(
            author.books.map(book => {         
              return queryInterface.bulkDelete('Books',
              [
               { title: book.title },
               { isbn: book.isbn }
              ]);
            })
           )
        })
         .catch(error => console.error(error));
      }));
    }
}