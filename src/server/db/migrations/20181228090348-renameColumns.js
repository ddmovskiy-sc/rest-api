'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return Promise.all([
    //   queryInterface.renameColumn('Authors', 'first_name', 'name'),
    //   queryInterface.renameColumn('Books', 'isbn', 'number')
    ])
  },

  down: (queryInterface, Sequelize) => {
      return Promise.all([
      queryInterface.renameColumn('Authors', 'name', 'first_name'),
      queryInterface.renameColumn('Books', 'number', 'isbn')
    ])   
  }
};
