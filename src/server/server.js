'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const router = require('./router');
const PORT = 3000;

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});

Promise.resolve().
  then(() => {
    return router(app);
  }).
  then(() => {
    new Promise((resolve, reject) => {
      app.listen(PORT, (error) => {
        if (!error) {
          console.log('Running on http://localhost:', PORT);
          return resolve();
        } else {
          return reject();
        }
      });
    });
  }).
  catch(error => console.error(error));
