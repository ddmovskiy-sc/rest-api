'use strict';

const db = require('../models');

module.exports = async () => {
  return {
    findAllByAuthor: async (authorId) => {
      const resultFind = await db.Book.findAll({ where: { authorId: authorId } });
      return resultFind.map((result) => result.dataValues);
    },
    findAll: async () => {
      const resultFind = await db.Book.findAll();
      return resultFind.map((result) => result.dataValues);
    },
    findById: async (bookId) => {
      const resultFind = await db.Book.findByPk(bookId);
      return resultFind.dataValues;
    },
    addBook: async (authorId, newBook) => {
       return db.Book.create({
        title: newBook.title,
        isbn: newBook.isbn,
        authorId: authorId
      });
    },
    updateById: async (bookId, updateBook) => {
      await db.Book.update({ 
        title: updateBook.title,
        isbn: updateBook.isbn
      },
      { where: { id: bookId }
      });
      return await db.Book.findByPk(bookId);
    },
    deleteById: async (bookId) => {
      return await db.Book.destroy({
        where: {
          id: bookId
        }
      });
    }
  };
};
