'use strict';

const db = require('../models');

module.exports = () => {
  return {
    findAll: async () => {
      const resultFind = await db.Author.findAll({include: [{ model: db.Book, as: 'books' }]})
      return resultFind.map((result) => result.dataValues);
    },
    findById: async (authorId) => {
      const resultFind = await db.Author.findByPk(authorId, {include: [{ model: db.Book, as: 'books' }]});
      return (resultFind) ? resultFind.dataValues : resultFind;
    },
    addAuthor: async (newAuthor) => {
      return db.Author.create({
        first_name: newAuthor.first_name,
        last_name: newAuthor.last_name,
        books: newAuthor.books
      },
      {
        include: [{ model: db.Book, as: 'books' }]
      })
    },
    updateById: async (authorId, updateAuthor) => {
      await db.Author.update({
        first_name: updateAuthor.first_name,
        last_name: updateAuthor.last_name
      },
      { where: { id: authorId }
      });
      return await db.Author.findByPk(authorId);
    },
    deleteById: async (authorId) => {
      return await db.Author.destroy({ where: { id: authorId } });
    },    
    findByField: async (condition) => {
      if ('isbn' in condition) {
         const book = await db.Book.findOne({ where: condition });
        return (book !== null) ? await db.Author.findOne({ where: { id: book.dataValues.authorId } }) : null;
      } else {
        const findResult = await db.Author.findAll({ where: condition });
        return (findResult.length > 0) ? findResult : null;
      }
    }
  }
}; 
 

